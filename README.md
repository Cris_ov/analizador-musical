# analizador musical

Mi idea principal es crear un analizador musical el cual sea capaz de obtener información de una canción:
1. Tempo
2. base ritmica
3. genero
4. instrumentos que lo componen
5. *letra*

Y de ahí partir de eso ser capaz de juntar las canciones según cada uno de los puntos anteriores.
Ejemplo:
- Hacer un playlist donde la base ritmica similiar. 
- Un playlist que junte todas las caracteristicas de una canción especifica.

Se quiere poder: 
- Consumir la música desde youtube - spotify - archivos locales
- crear una interfaz grafica capaz de mostrar los resultados

Se considera hacer una version web ya que es mas facil de trabajar lo visual.

